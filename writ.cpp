/*MIT License

Copyright (c) 2019 Jeriko Green

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE
*/

#include "writ.h"

using namespace tinyxml2;

ValidFileType getFileType(std::string string)
{
	auto to_lowercase = [](char& c)
	{
		c = std::tolower(static_cast<unsigned char>(c));
	};

	ValidFileType file_type;

	std::for_each(string.begin(), string.end(), to_lowercase);

	if (string == ".txt")
		file_type = ValidFileType::txt;
	else if(string == ".fodt")
		file_type = ValidFileType::fodt;
	else if(string == ".xml")
		file_type = ValidFileType::xml;
	else
		throw std::runtime_error("Invalid target file filetype.");

	return file_type;
}

Writ::Writ(const std::filesystem::path target_file_path, const std::filesystem::path manifest_path, const std::filesystem::path versions_path)
	: Writ(manifest_path, versions_path)
{
	if (!std::filesystem::exists(target_file_path))
		throw std::runtime_error("Invalid target path.");

	this->target_file_path = target_file_path.string();

	std::string extension = target_file_path.extension().string();
	getFileType(extension);
}

Writ::Writ(const std::filesystem::path manifest_path, const std::filesystem::path versions_path)
{
	std::setlocale(LC_ALL, "en_US.UTF-8");

	hasManifest = false;
	hasVersions = false;
	
	this->manifest_path = manifest_path.string();
	this->versions_path = versions_path.string();

	if (std::filesystem::exists(manifest_path))
		hasManifest = true;
	else
		std::filesystem::create_directories(manifest_path.parent_path());

	if (std::filesystem::exists(versions_path))
		hasVersions = true;
	else 
		std::filesystem::create_directories(versions_path.parent_path()); 
}

//load or create manifest and file
void Writ::initWrit()
{
	if(hasVersions && hasManifest) //manifest and versions present: load data for writ object
		loadWritData();
	else //corrupted install or no install: make new writ data
	{
		makeNewManifest();

		std::ofstream versions_stream(versions_path);

		if (!versions_stream)
			std::cerr << "Could not create versions file. Error: " << std::strerror(errno);

		versions_stream.close();
	}

	//if intializing with a target file to version or track
	if (!target_file_path.empty())
		saveVersion("Original Document");
}

void Writ::makeNewManifest()
{
	std::ofstream manifest_stream(manifest_path);

	if (!manifest_stream)
		std::cerr << "Could not create file. Error: " << std::strerror(errno);

	manifest_stream.close();
	manifest.InsertEndChild(manifest.NewDeclaration());
	root = manifest.InsertEndChild(manifest.NewElement("manifest:tracked-file"));
	manifest.SaveFile(manifest_path.c_str());//closes file stream
}

//deserialize versions history from file
void Writ::loadWritData()
{
	updateNextWriteByte();

	manifest.LoadFile(manifest_path.c_str());
	parseManifest();
}

void Writ::updateNextWriteByte()
{
	std::ifstream stream(versions_path, std::ios::binary | std::ios::ate);
	next_write_byte = stream.tellg();
	next_write_byte++;

	stream.close();
}

void Writ::parseManifest()
{
	std::fstream stream(manifest_path);
	std::string file_contents = std::string(std::istreambuf_iterator<char>(stream), std::istreambuf_iterator<char>());
	stream.close();

	manifest.Parse(file_contents.c_str());
	XMLHandle manifest_handle(&manifest);
	root = manifest_handle.LastChild().ToNode();

	Version version;
	int v_id;

	//iterate through manifest to create versions data
	for (XMLNode* v_node = root->FirstChild(); v_node; v_node = v_node->NextSibling())
	{
		v_node->FirstChildElement("version-number")->QueryIntText(&v_id);

		if (current_version < v_id)
			current_version = v_id;

		int v_start_byte;
		v_node->FirstChildElement("start-byte")->QueryIntText(&v_start_byte);
		version.start_byte = v_start_byte;
		version.patch_size = std::stol(v_node->FirstChildElement("patch-size")->GetText());
		version.description = v_node->FirstChildElement("version-description")->GetText();
		version.doc_name = v_node->FirstChildElement("document-name")->GetText();
		std::string filetype = v_node->FirstChildElement("document-type")->GetText();
		version.filetype = getFileType(filetype);
		version.hash = v_node->FirstChildElement("tracked-file:hash")->FirstChildElement("value")->GetText();
		versions.push_back(version);
	}

	std::cout << "Number of versions: " << versions.size() << std::endl;
}

void Writ::saveVersion(const std::string& description)
{
	saveVersion(description, target_file_path);
}

void Writ::saveVersion(const std::string& description, const std::filesystem::path full_file_path)
{
	XMLHandle manifest_handle(&manifest);
	root = manifest_handle.LastChild().ToNode();

	//validate target_file_path if assigning for the first time
	if (target_file_path.empty())
	{
		if (std::filesystem::exists(full_file_path))
		{
			target_file_path = full_file_path.string();
			std::string extension = full_file_path.extension().string();
			getFileType(extension);
		}
		else
			throw std::runtime_error("Invalid target path.");
	}

	Version version;

	//add entry to manifest
	XMLNode* new_version = root->InsertEndChild(manifest.NewElement("tracked-file:version"));
	current_version++;
	XMLElement* id_number = manifest.NewElement("version-number");
	new_version->InsertEndChild(id_number);
	id_number->SetText(current_version);

	XMLElement* start = manifest.NewElement("start-byte");
	new_version->InsertEndChild(start);
	std::string byte_str = std::to_string(next_write_byte);
	start->SetText(byte_str.c_str());
	version.start_byte = next_write_byte;

	XMLElement* size = manifest.NewElement("patch-size");
	new_version->InsertEndChild(size);
	unsigned long patch_size = 0L;

	XMLElement* desc = manifest.NewElement("version-description");
	new_version->InsertEndChild(desc);
	desc->SetText(description.c_str());
	version.description = description;

	std::string doc_name = full_file_path.filename().string();
	XMLElement* name = manifest.NewElement("document-name");
	new_version->InsertEndChild(name);
	name->SetText(doc_name.c_str());
	version.doc_name = doc_name;
	
	std::string extension = full_file_path.extension().string();
	XMLElement* type = manifest.NewElement("document-type");
	new_version->InsertEndChild(type);
	type->SetText(extension.c_str());
	version.filetype = getFileType(extension);

	//must open in binary mode in order for hash to work correctly
	std::ifstream file_version_stream(full_file_path, std::ios::binary);

	//store copy of original file for diffing purposes
	if (current_version == 0)
	{
		std::filesystem::path target(std::filesystem::path(versions_path).parent_path() / full_file_path.filename());
		std::filesystem::copy_file(full_file_path, target);
	}

	std::string file_contents = std::string(std::istreambuf_iterator<char>(file_version_stream), std::istreambuf_iterator<char>());
	std::string hash_string = picosha2::hash256_hex_string(file_contents);

	XMLNode* hash = new_version->InsertEndChild(manifest.NewElement("tracked-file:hash"));
	
	XMLElement* hash_type = manifest.NewElement("type");
	hash->InsertEndChild(hash_type);
	hash_type->SetText("SHA-256");

	XMLElement* hash_text = manifest.NewElement("value");
	hash->InsertEndChild(hash_text);
	hash_text->SetText(hash_string.c_str());
	version.hash = hash_string;

	if ( current_version > 0 )
	{
		patch_size = addVersionPatch(file_version_stream);
		updateNextWriteByte();
	}
	else
		next_write_byte = 0;

	std::string size_str = std::to_string(patch_size);
	size->SetText(size_str.c_str());
	version.patch_size = patch_size;

	manifest.SaveFile(manifest_path.c_str());
	versions.push_back(version);
}

unsigned long Writ::addVersionPatch(std::ifstream& file_version_stream)
{
	file_version_stream.clear();
	file_version_stream.seekg(0, std::ios::beg);
	std::string new_version { std::istreambuf_iterator<char>(file_version_stream), std::istreambuf_iterator<char>() };
	file_version_stream.close();

	std::string last_version;

	if (current_version == 1) //if diffing off original
		last_version = getOriginalDoc();
	else if (current_version > 1)
		last_version = getVersionText(current_version);
	
	std::string delta;

	open_vcdiff::VCDiffEncoder encoder(last_version.data(), last_version.size());
	encoder.SetFormatFlags(open_vcdiff::VCD_FORMAT_INTERLEAVED);
	encoder.Encode(new_version.data(), new_version.size(), &delta);

	std::ofstream versions_stream(versions_path, std::ios::out | std::ios::app);
	versions_stream << delta;
	versions_stream.close();

	return delta.size();
}

std::string Writ::getOriginalDoc()
{
	XMLElement* document_name = root->FirstChild()->FirstChildElement("document-name");
	std::string filename = document_name->GetText();

	std::filesystem::path original_doc_path(std::filesystem::path(versions_path).parent_path() / filename);
	original_doc_path.make_preferred();

	std::ifstream stream(original_doc_path);
	std::string file_text { std::istreambuf_iterator<char>(stream), std::istreambuf_iterator<char>() };
	stream.close();

	return file_text;
}

std::string Writ::getVersionText(int version_number)
{
	std::cout << "current version: " << current_version << std::endl;
	if (version_number < 0 || version_number > current_version)
		throw std::runtime_error("version_number out of range!");

	else if (version_number == 0)
		return getOriginalDoc();

	unsigned long patch_buffer = 0L;

	//find largest patch size to set buffer to
	for (int i = 0; i < versions.size(); i++) 
	{
		if (versions.at(i).patch_size > patch_buffer)
			patch_buffer = versions.at(i).patch_size;
	}

	std::cout << "patch_buffer: " << patch_buffer << std::endl;

	std::string patch;
	patch.resize(patch_buffer);

	//apply patches one by one
	std::ifstream versions_stream(versions_path, std::ios::binary);
	open_vcdiff::VCDiffDecoder decoder;
	std::string last_doc = getOriginalDoc();
	std::string generated_doc;
	std::streampos pos;

	//start from first version with delta
	int i = 1;
	do
	{
		if (!generated_doc.empty())
			last_doc = generated_doc;

		versions_stream.seekg(pos, std::ios::beg);
		std::streampos startpos = versions_stream.tellg();

		versions_stream.read(patch.data(), versions.at(i).patch_size);
		pos = versions_stream.tellg() - startpos;

		decoder.Decode(last_doc.data(), last_doc.size(), patch, &generated_doc);
		i++;
	} while (i < version_number);

	versions_stream.close();
	decoder.Decode(last_doc.data(), last_doc.size(), patch, &generated_doc);

	return generated_doc;
}