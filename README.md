![the writ logo](/writ-logo.png)


# writ
writ is a C++ version control library, specifically for literature. It is meant to be utilized by GUI-based programs that require text document verisioning features.


## How it works

Standard version control systems don't gel well with the way many writers of literature work with document files.

writ tracks files on an individual basis, rather than by directory. This allows version history to keep track of a document even if its name or extension changes.

## Dependencies

To build writ as a static library requires the following:

* [tinyxml2](https://github.com/leethomason/tinyxml2)
* [open-vcdiff](https://github.com/google/open-vcdiff)
* [PicoSHA2](https://github.com/okdshin/PicoSHA2)

## Usage

Before using writ, it must be initialized with a manifest and versions file. This can be done in one of two ways.

## 1. Tracking a file for the first time
```
#include "writ.h" 

Writ *writ = new Writ(
        "doc-name.fodt", //tracked file
        "manifest/path/doc-name-manifest",
        "versions/path/doc-name-versions");

writ->initWrit();
```
## 2. Loading existing writ data

Simply ignore the tracked file argument.
```
#include "writ.h" 

Writ *writ = new Writ(
        "manifest/path/doc-name-manifest",
        "versions/path/doc-name-versions");

writ->initWrit();
```

After which new versions can be submitted and tracked easily.
## 3. Submitting a new version

```
writ->saveVersion("version description", "revision1.fodt");
```

## 4. Retrieving a previous version
```
std::string version2 = writ->getVersionText(1);
```

writ uses a zero-based index, thus retrieving the original version of a document can be done as follows:

```
std::string original = writ->getVersionText(0);
```


## License

writ is licensed under the MIT license which can be read in the LICENSE file.