/*MIT License

Copyright (c) 2019 Jeriko Green

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE
*/

#pragma once

#include <cerrno>
#include <clocale>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <locale>
#include <string>
#include <vector>
#include <google/vcencoder.h>
#include <google/vcdecoder.h>
#include "picosha2.h"
#include "tinyxml2.h"

enum class ValidFileType
{
	txt,
	fodt,
	xml
};

struct Version
{
	unsigned long start_byte;
	unsigned long patch_size;
	std::string description;
	std::string doc_name;
	ValidFileType filetype;
	std::string hash;
};

class Writ
{
	public:
		Writ(const std::filesystem::path target_file_path, const std::filesystem::path manifest_path, const std::filesystem::path versions_path);
		Writ(const std::filesystem::path manifest_path, const std::filesystem::path versions_path);
		void initWrit();
		void saveVersion(const std::string& description, const std::filesystem::path full_file_path);	
		inline int currentVersion() const { return current_version; }
		std::string getVersionText(int version_number);
		std::string getOriginalDoc();

	private:
		tinyxml2::XMLDocument manifest;
		tinyxml2::XMLNode* root;
		std::string manifest_path;
		std::string versions_path;
		std::string target_file_path;
		std::vector<Version> versions;
		int current_version = -1;
		unsigned long next_write_byte = 0L;
		bool hasManifest;
		bool hasVersions;

		void loadWritData();
		void parseManifest();
		void makeNewManifest();
		void saveVersion(const std::string& description);
		unsigned long addVersionPatch(std::ifstream& file_version_stream);
		void updateNextWriteByte();
};
